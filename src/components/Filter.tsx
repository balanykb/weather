export const Filter: React.FC<FilterProps> = () => {
    return (
        <div className="filter">
            <span className="checkbox">Облачно</span>
            <span className="checkbox">Солнечно</span>
            <p className="custom-input">
                <label htmlFor="min-temperature">Минимальная температура</label>
                <input id="min-temperature" type="number" value=""/>
            </p>
            <p className="custom-input">
                <label htmlFor="max-temperature">Максимальная температура</label>
                <input id="min-temperature" type="number" value=""/>
            </p>
            <button disabled>Отфильтровать</button>
        </div>
    );
};

interface FilterProps {}
