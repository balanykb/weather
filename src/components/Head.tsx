export const Head: React.FC<HeadProps> = () => {
    return (
        <div className="head">
            <div className="icon rainy"></div>
            <div className="current-date">
                <p>воскресенье</p>
                <span>4 апреля</span>
            </div>
        </div>
    );
};

interface HeadProps {}
