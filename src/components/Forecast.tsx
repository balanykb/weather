export const Forecast: React.FC<ForecastProps> = () => {
    return (
        <div className="forecast">
            <div className="day rainy selected">
                <p>воскресенье</p>
                <span>28</span>
            </div>
            <div className="day cloudy">
                <p>понедельник</p>
                <span>23</span>
            </div>
            <div className="day rainy">
                <p>вторник</p>
                <span>17</span>
            </div>
            <div className="day sunny">
                <p>среда</p>
                <span>30</span>
            </div>
            <div className="day rainy">
                <p>четверг</p>
                <span>25</span>
            </div>
            <div className="day sunny">
                <p>пятница</p>
                <span>23</span>
            </div>
            <div className="day sunny">
                <p>суббота</p>
                <span>20</span>
            </div>
        </div>
    );
};

interface ForecastProps {}
