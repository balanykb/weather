export const CurrentWeather: React.FC<CurrentWeatherProps> = () => {
    return (
        <div className="current-weather">
            <p className="temperature">28</p>
            <p className="meta">
                <span className="rainy">%13</span>
                <span className="humidity">%42</span>
            </p>
        </div>
    );
};

interface CurrentWeatherProps {}
